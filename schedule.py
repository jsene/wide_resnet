import time 
import matplotlib.pyplot as plt
import os 
import numpy as np
import math

class Callback:
    def on_batch_end(self): pass


class LR_sched(Callback):
    def __init__(self, init_lr, params):
        self.Ti = params['Ti']
        self.f = lambda Tcur : 0.5 * init_lr * (1 + math.cos(Tcur/self.Ti * math.pi))
        self.dT = params['dT']
        self.mult_factor = params['mult_factor']
        self.Tcur = 0
        self.Ti_Next = self.Ti
        self.epochs = []


    def update_lr(self, epoch):
        cur_lr = self.f(self.Tcur)
        self.Tcur += self.dT

        if epoch >= self.Ti_Next:
            self.Tcur = 0
            self.Ti *= self.mult_factor
            self.Ti_Next += self.Ti

        return cur_lr 